# Computação Grafica 2
 Todos os projetos de computação grafica 2
 Separação realizada através de branches por terem o mesmo HTML base

## Exercicio 1 - Arvore de blocos
>  Branch: Arvore
### Descrição: 
Crie uma aplicação Bayblon conforme o vídeo apresentado:
[Link](https://www.youtube.com/watch?v=saJMLqV9aPQ)
	
## Exercicio 2 - Esferas
>  Branch: Esferas
### Descrição: 
Desenvolver uma aplicação Bayblon.js simular ao vídeo apresentado.
[Link]( https://www.youtube.com/watch?v=t2HR-9QXw8U&feature=youtu.be)

Observe o sincronismo e a simetria dos movimentos.
Dica: Tente usar Classe/Objeto pois facilita o desenvolvimento desse projeto.
Critérios de Avaliação:
1. Movimento correto das Esferas Internas (2 Pontos).
2. Movimento correto das Esferas Externas (2 Pontos).
3. Linhas Brancas Internas (2 Pontos).
4. Linhas Brancas Externas (2 Pontos).
5. Sincronismo das Esferas (2 Pontos).

## Exercicio 3 - Tabuleiro Basico
>  Branch: TabuleiroBasic
### Descrição: 
Implemente uma aplicação baseada no vídeo abaixo:
[Link](https://www.youtube.com/watch?v=3rZ9HaoM2sQ&feature=youtu.be)

A sua aplicação deverá ter :

1. A estrutura mínima conforme apresentada no vídeo (4 pontos).
2. Uma forma de escolher o tamanho inicial da estrutura, no vídeo o tamanho inicial é de 6 x 6 box ( 2 pontos).
3. Uma forma de alterar a textura aplicada ao box antes sua criação, o usuário deverá escolher entre pelo menos 3 tipos de texturas diferentes (2 pontos).
4. Uma forma para remover um box selecionado (2 pontos).

Observação: Cópias ou projetos semelhantes serão zerados!

## Exercicio 4 - Tabuleiro Modificado
>  Branch: TabuleiroModificado
### Descrição: 

Incluir duas novas funcionalidades na Atividade 03 ao seu critério.

As novas funcionalidades precisam estar alinhadas com os objetivos do projeto, que é construir e visualizar os cubos construídos.

Critérios de avaliação:

Cada nova funcionalidade será pontuada de 0 a 5, totalizando 10 pontos as duas novas funcionalidades.

Será avaliado o grau de dificuldade de cada nova funcionalidade e pontuado da seguinte forma

-   Simples até 1 ponto, com pouco impacto no projeto (pequenas alterações, poucas linhas incluídas, programação simples ou inexistente, etc...)
-   Média até 3 pontos, com impacto médio no projeto, porém ainda não essencial (várias alterações com muitas linhas, programação média com adição de classes e funções, interação do usuário, etc...)
-   Complexa até 5 pontos, com impacto alto no projeto e essencial para a construção e manipulação nos novos cubos. (inclusão de diversas classes e funções, interação do usuário por diversos teclado e mouse, uso de texturas e luz, animações, etc...)

## Exercicio 5 - Exercicio Final
>  Branch: Final
### Descrição: 
Construir um projeto com tema livre.

O projeto deve ser construído do zero.

Critérios de avaliação:

1. Funcionamento Correto (Sem erros) (Até 2 pontos).
2. Uso de material, texturas e luz (Até 1 ponto).
3. Interação do usuário com a aplicação usando mouse e/ou teclado (Até 2 pontos).
4. Animações e uso de diversos objetos na cena (Até 2 pontos).
5. Estruturação do código fonte (uso de classes e funções) (Até 1 ponto).
6. Tema do projeto (originalidade, dificuldade, usabilidade, etc, ) (Até 1 ponto).
7. Colocar uma opção de sobre e ajuda no projeto que deve ser apresentado quando o usuário pressionar a tecla M (Dentro da Cena Gráfica) (1 Ponto).

Informações necessárias:

Nome completo e matricula da dupla.

Objetivos.

Como usar (teclas, opções, botões do mouse, etc...) .

Demais informações necessárias.